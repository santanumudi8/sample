package com.example.spring.cache.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.example.spring.cache.api.dao.UserRepository;
import com.example.spring.cache.api.model.User;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;
	
	
	@Cacheable(cacheNames = {"userCache"})
	public List<User> getUsers(){
		System.out.println("Hit DB 1st time for getUsers()");
		List<User> users = userRepository.findAll();
		return users;
	}
	
	@Cacheable(value = "userCache", key = "#id", unless = "#result==null")
	public User getUser(int id) {
		System.out.println("Hit DB 1st time for getUser(id)");
		User user = userRepository.findById(id).get();
		return user;
	}
	
	@CacheEvict(value = "userCache")
	public String delete(int id) {
		userRepository.deleteById(id);
		return "User deleted with id : "+id;
	}
	
}
